using System;
using System.Collections.Generic;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Views;
using Android.Widget;
using OfficeTools.Shared.Models;
using V7Toolbar = Android.Support.V7.Widget.Toolbar;
using OfficeTools.Employees.Droid.Classes;
using Com.Nostra13.Universalimageloader.Core;
using Android.Support.V7.Widget;

namespace OfficeTools.Employees.Droid.Views
{
    [Activity(Theme = "@style/Theme.DesignDemo", Label = "DashboardActivity")]
    public class DashboardActivity : NavigationDrawerActivity
    {
        private Dashboard dashboardDetails = new Dashboard();
        //new employee
        private TextView newEmployeeNameText;
        private TextView newEmployeeEmailText;
        private ImageView newEmployeeImage;
        RecyclerView anniversaryRecyclerView;
        RecyclerView birthdayRecyclerView;
        DisplayImageOptions displayImageOption;
        //spotlight employee
        private TextView spotlightEmployeeNameText;
        private TextView spotlightEmployeeEmailText;
        private ImageView spotlightEmployeeImage;
        private TextView anniverssaryTitleText;
        private TextView birthdayTitleText;
        private TextView holidaysTitleText;
        private RecyclerView holidayRecyclerView;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.DashboradLayout);
            Set(Resources.GetStringArray(Resource.Array.nav_drawer_items), Resources.ObtainTypedArray(Resource.Array.nav_drawer_icons));
            var toolbar = FindViewById<V7Toolbar>(Resource.Id.toolbar);
            SetSupportActionBar(toolbar);
            TextView toolbarTitle = FindViewById<TextView>(Resource.Id.toolbarTitle);
            toolbarTitle.Text = Resources.GetString(Resource.String.Dashboard);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);
            SupportActionBar.SetDisplayShowTitleEnabled(false);
            SupportActionBar.SetHomeButtonEnabled(true);
            SupportActionBar.SetHomeAsUpIndicator(Resource.Drawable.ic_hamburger);
            var config = ImageLoaderConfiguration.CreateDefault(ApplicationContext);
            ImageLoader.Instance.Init(config);

            birthdayTitleText = FindViewById<TextView>(Resource.Id.birthdayTitleText);
            holidaysTitleText = FindViewById<TextView>(Resource.Id.holidaysTitleText);
            anniverssaryTitleText = FindViewById<TextView>(Resource.Id.anniverssaryTitleText);
            newEmployeeNameText = FindViewById<TextView>(Resource.Id.newEmployeeNameText);
            newEmployeeEmailText= FindViewById<TextView>(Resource.Id.newEmployeeEmailText);
            newEmployeeImage = FindViewById<ImageView>(Resource.Id.newEmployeeImage);

            spotlightEmployeeNameText = FindViewById<TextView>(Resource.Id.spotlightEmployeeNameText);
            spotlightEmployeeEmailText = FindViewById<TextView>(Resource.Id.spotlightEmployeeEmailText);
            spotlightEmployeeImage = FindViewById<ImageView>(Resource.Id.spotlightEmployeeImage);
            anniverssaryTitleText.Click += AnniverssaryTitleText_Click;
            birthdayTitleText.Click += BirthdayTitleText_Click;
            holidaysTitleText.Click += HolidaysTitleText_Click;
            if (AppUtil.IsNetworAvailable(this))
            {

                dashboardDetails = new Repositories.EmployeeRepository(this).GetDashboardDetails().data;
                ImageLoader imageLoader = ImageLoader.Instance;
                displayImageOption = new DisplayImageOptions.Builder().CacheOnDisk(true)
                .Build();
                
                
                newEmployeeNameText.Text=dashboardDetails.NewEmployee.FirstName;
                newEmployeeEmailText.Text = dashboardDetails.NewEmployee.Email;
                imageLoader.DisplayImage(dashboardDetails.NewEmployee.profileImage, newEmployeeImage,
                    displayImageOption, null);

                spotlightEmployeeNameText.Text = dashboardDetails.SpotlightEmployee.FirstName;
                spotlightEmployeeEmailText.Text = dashboardDetails.SpotlightEmployee.Email;
                imageLoader.DisplayImage(dashboardDetails.SpotlightEmployee.ProfileImage, spotlightEmployeeImage,
                    displayImageOption, null);


                birthdayRecyclerView = FindViewById<RecyclerView>(Resource.Id.recyclerView);
                birthdayRecyclerView.NestedScrollingEnabled=false;
                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this,LinearLayoutManager.Horizontal,false);
                birthdayRecyclerView.SetLayoutManager(mLayoutManager);
                BirthdayAdapter birthdayAdapter = new BirthdayAdapter(dashboardDetails.UpcomingBirthdays, this);
                birthdayRecyclerView.SetAdapter(birthdayAdapter);
                anniversaryRecyclerView = FindViewById<RecyclerView>(Resource.Id.birthdayRecyclerView);
                RecyclerView.LayoutManager anniversaryLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.Horizontal, false);

                anniversaryRecyclerView.SetLayoutManager(anniversaryLayoutManager);
                AnniversaryAdapter anniversaryAdapter = new AnniversaryAdapter(dashboardDetails.UpcomingAnniversaries, this);
                anniversaryRecyclerView.SetAdapter(anniversaryAdapter);

                holidayRecyclerView = FindViewById<RecyclerView>(Resource.Id.holidayRecyclerView);
                //holidayRecyclerView.NestedScrollingEnabled = false;
                RecyclerView.LayoutManager holidayLayoutManager = new LinearLayoutManager(this);
                holidayRecyclerView.SetLayoutManager(holidayLayoutManager);
                HolidayAdapter holidayAdapter = new HolidayAdapter(dashboardDetails.UpcomingHolidays, this);
                holidayRecyclerView.SetAdapter(holidayAdapter);

                RecyclerView eventsRecyclerView = FindViewById<RecyclerView>(Resource.Id.eventsRecyclerView);
               // eventsRecyclerView.NestedScrollingEnabled = false;
                RecyclerView.LayoutManager eventsLayoutManager = new LinearLayoutManager(this);
                eventsRecyclerView.SetLayoutManager(eventsLayoutManager);
                EventsAdapter eventsAdapter = new EventsAdapter(dashboardDetails.UpcomingEvents, this);
                eventsRecyclerView.SetAdapter(eventsAdapter);
            }
        }

        private void HolidaysTitleText_Click(object sender, EventArgs e)
        {
            expandableView(holidayRecyclerView);
        }

        private void BirthdayTitleText_Click(object sender, EventArgs e)
        {
            expandableView(birthdayRecyclerView);
        }

        private void AnniverssaryTitleText_Click(object sender, EventArgs e)
        {
            expandableView(anniversaryRecyclerView);
        }



        public void expandableView(View view)
        {
            if (view.Visibility.ToString().Equals("Visible"))
                view.Visibility = ViewStates.Gone;
            else
            {
                view.Visibility = ViewStates.Visible;
                       
            } 
        }

        public class BirthdayViewHolder : RecyclerView.ViewHolder
        {
            public ImageView ProfileImage { get; private set; }

            public TextView FirstName { get; private set; }

            public TextView Designation { get; private set; }


            public BirthdayViewHolder(View itemView, Action<int> listener)
                : base(itemView)
            {

                ProfileImage = itemView.FindViewById<ImageView>(Resource.Id.imageView);
                FirstName = itemView.FindViewById<TextView>(Resource.Id.textView);
                Designation = itemView.FindViewById<TextView>(Resource.Id.designationText);
                itemView.Click += (sender, e) => listener(base.AdapterPosition);
            }


        }

        public class BirthdayAdapter : RecyclerView.Adapter
        {
            public event EventHandler<int> ItemClick;
            public List<UpcomingBirthday> upcomingBirthday;
            public Context context;

            public BirthdayAdapter(List<UpcomingBirthday> upcomingBirthday, Context context)
            {
                this.upcomingBirthday = upcomingBirthday;
                this.context = context;
            }


            public override RecyclerView.ViewHolder
                OnCreateViewHolder(ViewGroup parent, int viewType)
            {
                View itemView = LayoutInflater.From(parent.Context).
                            Inflate(Resource.Layout.RecyclerBirthdayItems, parent, false);
                BirthdayViewHolder vh = new BirthdayViewHolder(itemView, OnClick);
                return vh;
            }

            public override int GetItemViewType(int position)
            {
                return position;
            }
            public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
            {
                BirthdayViewHolder vh = holder as BirthdayViewHolder;
                DisplayImageOptions displayImageOption;
                var emp = upcomingBirthday[position];
                ImageLoader imageLoader = ImageLoader.Instance;
                displayImageOption = new DisplayImageOptions.Builder().CacheOnDisk(true)
                .Build();
                imageLoader.DisplayImage(emp.ProfileImage, vh.ProfileImage,
                    displayImageOption, null);
                vh.FirstName.Text = emp.FirstName + " " + emp.LastName;
                vh.Designation.Text = Convert.ToDateTime(emp.BirthDate).ToString("MMM-dd yyyy");
                
                
            }

            public override int ItemCount
            {
                get { return upcomingBirthday.Count; }
            }


            void OnClick(int position)
            {
                ItemClick?.Invoke(this, position);
            }
        }



        public class HolidayViewHolder : RecyclerView.ViewHolder
        {
            public TextView FirstName { get; private set; }

            public TextView Designation { get; private set; }


            public HolidayViewHolder(View itemView, Action<int> listener)
                : base(itemView)
            {

                FirstName = itemView.FindViewById<TextView>(Resource.Id.textView);
                Designation = itemView.FindViewById<TextView>(Resource.Id.designationText);
                itemView.Click += (sender, e) => listener(base.AdapterPosition);
            }


        }



        public class HolidayAdapter : RecyclerView.Adapter
        {
            public event EventHandler<int> ItemClick;
            public List<UpcomingHoliday> upcomingBirthday;
            public Context context;

            public HolidayAdapter(List<UpcomingHoliday> upcomingBirthday, Context context)
            {
                this.upcomingBirthday = upcomingBirthday;
                this.context = context;
            }


            public override RecyclerView.ViewHolder
                OnCreateViewHolder(ViewGroup parent, int viewType)
            {
                View itemView = LayoutInflater.From(parent.Context).
                            Inflate(Resource.Layout.RecyclerHolidayItems, parent, false);
                HolidayViewHolder vh = new HolidayViewHolder(itemView, OnClick);
                return vh;
            }

            public override int GetItemViewType(int position)
            {
                return position;
            }
            public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
            {
                HolidayViewHolder vh = holder as HolidayViewHolder;
                
                var emp = upcomingBirthday[position];
                vh.FirstName.Text = emp.Name ;
                vh.Designation.Text = Convert.ToDateTime(emp.Date).ToString("MMM-dd yyyy");


            }

            public override int ItemCount
            {
                get { return upcomingBirthday.Count; }
            }


            void OnClick(int position)
            {
                ItemClick?.Invoke(this, position);
            }
        }


        public class EventsViewHolder : RecyclerView.ViewHolder
        {
            public TextView FirstName { get; private set; }

            public TextView Designation { get; private set; }


            public EventsViewHolder(View itemView, Action<int> listener)
                : base(itemView)
            {

                FirstName = itemView.FindViewById<TextView>(Resource.Id.textView);
                Designation = itemView.FindViewById<TextView>(Resource.Id.designationText);
                itemView.Click += (sender, e) => listener(base.AdapterPosition);
            }


        }



        public class EventsAdapter : RecyclerView.Adapter
        {
            public event EventHandler<int> ItemClick;
            public List<UpcomingEvent> upcomingBirthday;
            public Context context;

            public EventsAdapter(List<UpcomingEvent> upcomingBirthday, Context context)
            {
                this.upcomingBirthday = upcomingBirthday;
                this.context = context;
            }


            public override RecyclerView.ViewHolder
                OnCreateViewHolder(ViewGroup parent, int viewType)
            {
                View itemView = LayoutInflater.From(parent.Context).
                            Inflate(Resource.Layout.RecyclerEventsItems, parent, false);
                EventsViewHolder vh = new EventsViewHolder(itemView, OnClick);
                return vh;
            }

            public override int GetItemViewType(int position)
            {
                return position;
            }
            public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
            {
                EventsViewHolder vh = holder as EventsViewHolder;
                var emp = upcomingBirthday[position];
                vh.FirstName.Text = emp.Title;
                vh.Designation.Text = Convert.ToDateTime(emp.EventDate).ToString("MMM-dd yyyy");
             }

            public override int ItemCount
            {
                get { return upcomingBirthday.Count; }
            }


            void OnClick(int position)
            {
                ItemClick?.Invoke(this, position);
            }
        }

        
        public class AnniversaryViewHolder : RecyclerView.ViewHolder
        {
            public ImageView ProfileImage { get; private set; }

            public TextView FirstName { get; private set; }

            public TextView Designation { get; private set; }


            public AnniversaryViewHolder(View itemView, Action<int> listener)
                : base(itemView)
            {

                ProfileImage = itemView.FindViewById<ImageView>(Resource.Id.imageView);
                FirstName = itemView.FindViewById<TextView>(Resource.Id.textView);
                Designation = itemView.FindViewById<TextView>(Resource.Id.designationText);
                itemView.Click += (sender, e) => listener(base.AdapterPosition);
            }


        }

        public class AnniversaryAdapter : RecyclerView.Adapter
        {
            public event EventHandler<int> ItemClick;
            public List<UpcomingAnniversary> upcomingAnniversary;
            public Context context;

            public AnniversaryAdapter(List<UpcomingAnniversary> upcomingAnniversary, Context context)
            {
                this.upcomingAnniversary = upcomingAnniversary;
                this.context = context;
            }


            public override RecyclerView.ViewHolder
                OnCreateViewHolder(ViewGroup parent, int viewType)
            {
                View itemView = LayoutInflater.From(parent.Context).
                            Inflate(Resource.Layout.RecyclerAnniversaryItems, parent, false);
                BirthdayViewHolder vh = new BirthdayViewHolder(itemView, OnClick);
                return vh;
            }

            public override int GetItemViewType(int position)
            {
                return position;
            }
            public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
            {
                BirthdayViewHolder vh = holder as BirthdayViewHolder;
                DisplayImageOptions displayImageOption;
                var emp = upcomingAnniversary[position];
                ImageLoader imageLoader = ImageLoader.Instance;
                displayImageOption = new DisplayImageOptions.Builder().CacheOnDisk(true)
                .Build();
                imageLoader.DisplayImage(emp.ProfileImage, vh.ProfileImage,
                    displayImageOption, null);
                vh.FirstName.Text = emp.FirstName + " " + emp.LastName;
                vh.Designation.Text = Convert.ToDateTime(emp.JoiningDate).ToString("MMM-dd yyyy"); 

            }

            public override int ItemCount
            {
                get { return upcomingAnniversary.Count; }
            }


            void OnClick(int position)
            {
                ItemClick?.Invoke(this, position);
            }
        }

        
        public override void OnBackPressed()
        {
            AlertDialog.Builder alert = new AlertDialog.Builder(this);
            alert.SetTitle(Resource.String.confirm_log_out);
            alert.SetMessage(Resource.String.confirm_log_out_enquiry);
            alert.SetPositiveButton(Resource.String.Yes, (senderAlert, args) =>
            {
                AppPreferences.SaveAccessToken("");
                Intent i = new Intent(this, typeof(LoginActivity));
                StartActivity(i);
                Finish();

            });
            alert.SetNegativeButton(Resource.String.No, (senderAlert, args) =>
            {
                CustomToast customMessage = new CustomToast(this, this, GetString(Resource.String.CancelLogout), true);
                customMessage.SetGravity(GravityFlags.Top, 0, 0);
                customMessage.Show();

            });
            Dialog dialog = alert.Create();
            dialog.Show();

        }

    }
}
