using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using OfficeTools.Employees.Droid.Adapters;
using OfficeTools.Employees.Droid.Classes;
using OfficeTools.Shared.Models;

namespace OfficeTools.Employees.Droid.Views
{
    [Activity(Theme = "@style/Theme.DesignDemo", Label = "ActivityRough")]
    public class ActivityRough : Activity
    {
        BirthdayListAdapter birthdayListAdapter;
        AnniversaryListAdapter anniversaryListAdapter;
        ExpandableListView birthdayListView,annivarsaryListView;
        List<string> listDataHeader;
        Dictionary<string, List<UpcomingBirthday>> listDataChild;
        Dictionary<string, List<UpcomingAnniversary>> anivversarylistDataChild;
        int previousGroup = -1;
        private Dashboard dashboardDetails = new Dashboard();

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.DashboardScreenLayout);
            if (AppUtil.IsNetworAvailable(this))
            {
                dashboardDetails = new Repositories.EmployeeRepository(this).GetDashboardDetails().data;
            }
            birthdayListView = FindViewById<ExpandableListView>(Resource.Id.lvExp);
            annivarsaryListView = FindViewById<ExpandableListView>(Resource.Id.anniversaryListView);

            // Prepare list data
            BirthdayGetListData();
            birthdayListAdapter = new BirthdayListAdapter(this, listDataHeader, listDataChild);


            birthdayListView.SetAdapter(birthdayListAdapter);
            AnniversayListData();
            //Bind list
            anniversaryListAdapter = new AnniversaryListAdapter(this, listDataHeader, anivversarylistDataChild);
            annivarsaryListView.SetAdapter(anniversaryListAdapter);

            FnClickEvents();
        }
        void FnClickEvents()
        {
            
            birthdayListView.ChildClick += delegate (object sender, ExpandableListView.ChildClickEventArgs e) {
                Toast.MakeText(this, "child clicked", ToastLength.Short).Show();
            };
            
            birthdayListView.GroupExpand += delegate (object sender, ExpandableListView.GroupExpandEventArgs e) {

                if (e.GroupPosition != previousGroup)
                    birthdayListView.CollapseGroup(previousGroup);
                previousGroup = e.GroupPosition;
            };
            
            birthdayListView.GroupCollapse += delegate (object sender, ExpandableListView.GroupCollapseEventArgs e)
            {
                Toast.MakeText(this, "group collapsed", ToastLength.Short).Show();
            };



        }
        void BirthdayGetListData()
        {
            listDataChild = new Dictionary<string, List<UpcomingBirthday>>();
            listDataHeader = new List<string>();
            listDataHeader.Add("Upcoming Birthdays");
            listDataChild.Add(listDataHeader[0], dashboardDetails.UpcomingBirthdays);
          }
        void AnniversayListData()
        {
            anivversarylistDataChild = new Dictionary<string, List<UpcomingAnniversary>>();
            listDataHeader = new List<string>();
            listDataHeader.Add("Upcoming Anniversaries");
            anivversarylistDataChild.Add(listDataHeader[0], dashboardDetails.UpcomingAnniversaries);
        }

    }
}