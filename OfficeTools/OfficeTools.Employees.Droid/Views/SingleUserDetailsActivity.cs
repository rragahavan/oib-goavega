using Android.App;
using Android.Content;
using Android.OS;
using Android.Views;
using Android.Views.Animations;
using Android.Widget;
using Com.Nostra13.Universalimageloader.Core;
using OfficeTools.Employees.Droid.Classes;
using OfficeTools.Shared;
using OfficeTools.Shared.Common;
using System;
using System.Globalization;
using V7Toolbar = Android.Support.V7.Widget.Toolbar;

namespace OfficeTools.Employees.Droid.Views
{
    [Activity(Theme = "@style/Theme.DesignDemo", Label = "SingleUserDetailsActivity")]
    public class SingleUserDetailsActivity : NavigationDrawerActivity
    {

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.SingleUserDetails);
            var toolbar = FindViewById<V7Toolbar>(Resource.Id.toolbar);
            SetSupportActionBar(toolbar);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);
            SupportActionBar.SetDisplayShowTitleEnabled(false);
            SupportActionBar.SetHomeButtonEnabled(true);
            SupportActionBar.SetHomeAsUpIndicator(Resource.Drawable.ic_hamburger);
            TextView toolbarTitle = FindViewById<TextView>(Resource.Id.toolbarTitle);
            toolbarTitle.Text = Resources.GetString(Resource.String.EmployeeDetails);
            Set(Resources.GetStringArray(Resource.Array.nav_drawer_items), Resources.ObtainTypedArray(Resource.Array.nav_drawer_icons));
            TextView nameText = FindViewById<TextView>(Resource.Id.NameText);
            TextView cityText = FindViewById<TextView>(Resource.Id.cityText);
            TextView zipText = FindViewById<TextView>(Resource.Id.zipText);
            TextView employeeIdText = FindViewById<TextView>(Resource.Id.employeeIdText);
            TextView roleText = FindViewById<TextView>(Resource.Id.roleText);
            TextView joiningDateText = FindViewById<TextView>(Resource.Id.joiningDateText);
            TextView addressText = FindViewById<TextView>(Resource.Id.addressText);
            TextView emailAddressText = FindViewById<TextView>(Resource.Id.emailAddressText);
            TextView stateText = FindViewById<TextView>(Resource.Id.stateText);
            ImageView profileImage = FindViewById<ImageView>(Resource.Id.profileImage);
            var config = ImageLoaderConfiguration.CreateDefault(ApplicationContext);
            ImageLoader.Instance.Init(config);
            string empCode = Intent.GetStringExtra("empcode");
            if (AppUtil.IsNetworAvailable(this))
            {
                var response = new Repositories.EmployeeRepository(this).GetEmployeeDetails(empCode);
                if (!response.Success)
                    return;
                Employee employee = response.Data;
                addressText.Text = employee.PermanentAddress?.Address;
                cityText.Text = employee.PermanentAddress?.City;
                zipText.Text = employee.PermanentAddress?.Zip;
                stateText.Text = employee.PermanentAddress?.State;
                nameText.Text = employee.FirstName + employee.LastName;
                roleText.Text = employee.JobTitle;
                joiningDateText.Text = employee.JoiningDate.Substring(0, 10);
                //  string monthName = new DateTime(2010 - 8 - 1).ToString("MMM", CultureInfo.InvariantCulture);
                employeeIdText.Text = "Employee Id : " + employee.EmpCode.ToString();
                emailAddressText.Text = employee.Email;
                ImageLoader imageLoader = ImageLoader.Instance;
                imageLoader.DisplayImage(employee.ProfileImage, profileImage);
                //Animation anim = AnimationUtils.LoadAnimation(ApplicationContext,
                //Resource.Animation.scale_animation);
                //profileImage.StartAnimation(anim);
            }
            else
            {
                CustomToast customMessage = new CustomToast(this, this, GetString(Resource.String.NoInternetConnection), true);
                customMessage.SetGravity(GravityFlags.Top, 0, 0);
                customMessage.Show();
            }
        }
    }
}