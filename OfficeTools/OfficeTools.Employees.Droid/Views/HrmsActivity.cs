using Android.App;
using Android.Content;
using Android.OS;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using Com.Nostra13.Universalimageloader.Core;
using OfficeTools.Employees.Droid.Classes;
using OfficeTools.Shared;
using OfficeTools.Shared.Common;
using Uri = Android.Net.Uri;
using System;
using System.Collections.Generic;
using V7Toolbar = Android.Support.V7.Widget.Toolbar;

namespace OfficeTools.Employees.Droid.Views
{
    [Activity(Theme = "@style/Theme.DesignDemo", Label = "HrmsActivity")]
    public class HrmsActivity : NavigationDrawerActivity
    {
        private List<Employee> employeeDetails = new List<Employee>();

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.HrmsLayout);
            Set(Resources.GetStringArray(Resource.Array.nav_drawer_items), Resources.ObtainTypedArray(Resource.Array.nav_drawer_icons));
            var toolbar = FindViewById<V7Toolbar>(Resource.Id.toolbar);
            SetSupportActionBar(toolbar);
            TextView toolbarTitle = FindViewById<TextView>(Resource.Id.toolbarTitle);
            toolbarTitle.Text = Resources.GetString(Resource.String.Hrms);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);
            SupportActionBar.SetDisplayShowTitleEnabled(false);
            SupportActionBar.SetHomeButtonEnabled(true);
            SupportActionBar.SetHomeAsUpIndicator(Resource.Drawable.ic_hamburger);
            var config = ImageLoaderConfiguration.CreateDefault(ApplicationContext);
            ImageLoader.Instance.Init(config);
            if (AppUtil.IsNetworAvailable(this))
            {
                employeeDetails = new Repositories.EmployeeRepository(this).GetEmployeeList().Data;
                RecyclerView mRecyclerView = FindViewById<RecyclerView>(Resource.Id.recyclerView);
                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
                mRecyclerView.SetLayoutManager(mLayoutManager);
                EmployeeAdapter mAdapter = new EmployeeAdapter(employeeDetails, this);
                mAdapter.ItemClick += OnItemClick;
                mRecyclerView.SetAdapter(mAdapter);
            }
            else
            {
                CustomToast customMessage = new CustomToast(this, this, GetString(Resource.String.NoInternetConnection), true);
                customMessage.SetGravity(GravityFlags.Top, 0, 0);
                customMessage.Show();
            }
        }

        void OnItemClick(object sender, int position)
        {

            Intent i = new Intent(this, typeof(SingleUserDetailsActivity));
            i.PutExtra("position", position);
            i.PutExtra("empcode", employeeDetails[position].EmpCode);
            StartActivity(i);
          //  OverridePendingTransition(Resource.Animation.swing_up_left, Resource.Animation.fade_out);
        }

        public class EmployeeViewHolder : RecyclerView.ViewHolder
        {
            public ImageView ProfileImage { get; private set; }

            public TextView FirstName { get; private set; }

            public TextView Designation { get; private set; }

            public TextView EmailAddress { get; private set; }

            public TextView MobileNumber { get; private set; }

            public EmployeeViewHolder(View itemView, Action<int> listener)
                : base(itemView)
            {

                ProfileImage = itemView.FindViewById<ImageView>(Resource.Id.imageView);
                FirstName = itemView.FindViewById<TextView>(Resource.Id.textView);
                Designation = itemView.FindViewById<TextView>(Resource.Id.designationText);
                EmailAddress = itemView.FindViewById<TextView>(Resource.Id.emailAddressText);
                MobileNumber = itemView.FindViewById<TextView>(Resource.Id.mobileText);
                itemView.Click += (sender, e) => listener(base.AdapterPosition);
            }


        }

        public class EmployeeAdapter : RecyclerView.Adapter
        {
            public event EventHandler<int> ItemClick;
            public List<Employee> mEmployee;
            public Context context;
            
            public EmployeeAdapter(List<Employee> employee, Context context)
            {
                mEmployee = employee;
                this.context = context;
            }


            public override RecyclerView.ViewHolder
                OnCreateViewHolder(ViewGroup parent, int viewType)
            {
                View itemView = LayoutInflater.From(parent.Context).
                            Inflate(Resource.Layout.RecyclerItems, parent, false);
                EmployeeViewHolder vh = new EmployeeViewHolder(itemView, OnClick);
                return vh;
            }

            public override int GetItemViewType(int position)
            {
                return position;
            }
            public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
            {
                EmployeeViewHolder vh = holder as EmployeeViewHolder;
                DisplayImageOptions displayImageOption;
                var emp = mEmployee[position];
                ImageLoader imageLoader = ImageLoader.Instance;
                displayImageOption = new DisplayImageOptions.Builder().CacheOnDisk(true)
                .Build();
                imageLoader.DisplayImage(emp.ProfileImage, vh.ProfileImage,
                    displayImageOption, null);
                vh.FirstName.Text = emp.FirstName + " " + emp.LastName;
                vh.Designation.Text = emp.JobTitle;
                vh.EmailAddress.Text = emp.Email;
                vh.MobileNumber.Text = emp.Mobile;
               
                vh.MobileNumber.Click += delegate
                {
                    Intent callIntent = new Intent(Intent.ActionDial);
                    //Intent callIntent = new Intent(Intent.ActionCall);
                    callIntent.SetData(Uri.Parse("tel:" + mEmployee[position].Mobile));
                    context.StartActivity(callIntent);                    
                    
                };


            }
      
            public override int ItemCount
            {
                get { return mEmployee.Count; }
            }


            void OnClick(int position)
            {
                ItemClick?.Invoke(this, position);
            }
        }

        public override void OnBackPressed()
        {
            AlertDialog.Builder alert = new AlertDialog.Builder(this);
            alert.SetTitle(Resource.String.confirm_log_out);
            alert.SetMessage(Resource.String.confirm_log_out_enquiry);
            alert.SetPositiveButton(Resource.String.Yes, (senderAlert, args) =>
            {
                AppPreferences.SaveAccessToken("");
                Intent i = new Intent(this, typeof(LoginActivity));
                StartActivity(i);
                Finish();

            });
            alert.SetNegativeButton(Resource.String.No, (senderAlert, args) =>
             {
                 CustomToast customMessage = new CustomToast(this, this, GetString(Resource.String.CancelLogout), true);
                 customMessage.SetGravity(GravityFlags.Top, 0, 0);
                 customMessage.Show();
                
             });
            Dialog dialog = alert.Create();
            dialog.Show();

        }

    }
}
