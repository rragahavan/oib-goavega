using Android.App;
using Android.OS;
using Android.Widget;
using V7Toolbar = Android.Support.V7.Widget.Toolbar;
using Android.Support.V4.App;
using Android.Support.V4.View;
using OfficeTools.Employees.Droid.Adapters;
using OfficeTools.Employees.Droid.Classes;
using Android.Support.Design.Widget;
using Android.Content;
using Android.Views;
using System.Collections.Generic;

namespace OfficeTools.Employees.Droid.Views
{
    [Activity(Theme = "@style/Theme.DesignDemo", Label = "LeaveManagementActivity")]
    public class LeaveManagementActivity : NavigationDrawerActivity
    {
        FragmentPagerAdapter myPagerAdapter;
        private string[] tabTitles = { "My Leave", "Apply Leave", "My Summary", "Leave Request" };
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.LeaveTabLayout);
            Set(Resources.GetStringArray(Resource.Array.nav_drawer_items), Resources.ObtainTypedArray(Resource.Array.nav_drawer_icons));
            var toolbar = FindViewById<V7Toolbar>(Resource.Id.toolbar);
            SetSupportActionBar(toolbar);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);
            SupportActionBar.SetDisplayShowTitleEnabled(false);
            SupportActionBar.SetHomeButtonEnabled(true);
            SupportActionBar.SetHomeAsUpIndicator(Resource.Drawable.ic_hamburger);
            TextView toolbarTitle = FindViewById<TextView>(Resource.Id.toolbarTitle);
            toolbarTitle.Text = Resources.GetString(Resource.String.Leaves);
            ViewPager viewPager = FindViewById<ViewPager>(Resource.Id.viewPager);
            if (AppUtil.IsNetworAvailable(this))
            {
                var leaveDetails = new Repositories.EmployeeRepository(this).GetLeaveDetails().Data.ManagerleaveRequests;
                if (leaveDetails.Count == 0)
                {
                    var list = new List<string>(tabTitles);
                    list.Remove("Leave Request");
                    tabTitles = list.ToArray();
                }
            }
            myPagerAdapter = new LeavePagerAdapter(SupportFragmentManager, tabTitles, this);
            viewPager.Adapter = myPagerAdapter;
            myPagerAdapter.NotifyDataSetChanged();
            viewPager.SetPageTransformer(true, new ScaleTransformer());
            TabLayout tabLayout = FindViewById<TabLayout>(Resource.Id.sliding_tabs);
            tabLayout.SetupWithViewPager(viewPager);
            tabLayout.SetTabTextColors(Android.Graphics.Color.White, Android.Graphics.Color.White);
            tabLayout.TabSelected += (object sender, TabLayout.TabSelectedEventArgs e) =>
            {
                var tab = e.Tab;
                switch (tab.Position)
                {
                    case 0:
                        viewPager.SetCurrentItem(0, true);
                        toolbarTitle.Text = "My Leaves";

                        break;
                    case 1:
                        viewPager.SetCurrentItem(1, true);
                        toolbarTitle.Text = "Apply Leave";

                        break;
                    case 2:
                        viewPager.SetCurrentItem(2, true);
                        toolbarTitle.Text = "Leave Summary";

                        break;

                    case 3:
                        viewPager.SetCurrentItem(3, true);
                        toolbarTitle.Text = "Leave Requests";
                        break;
                    default:
                        toolbarTitle.Text = "Leaves";
                        break;
                }


            };


        }

        public override void OnBackPressed()
        {
            AlertDialog.Builder alert = new AlertDialog.Builder(this);
            alert.SetTitle(Resource.String.confirm_log_out);
            alert.SetMessage(Resource.String.confirm_log_out_enquiry);
            alert.SetPositiveButton(Resource.String.Yes, (senderAlert, args) =>
            {
                AppPreferences.SaveAccessToken("");
                Intent i = new Intent(this, typeof(LoginActivity));
                StartActivity(i);
                Finish();

            });
            alert.SetNegativeButton(Resource.String.No, (senderAlert, args) =>
            {
                CustomToast customMessage = new CustomToast(this, this, GetString(Resource.String.CancelLogout), true);
                customMessage.SetGravity(GravityFlags.Top, 0, 0);
                customMessage.Show();
            });
            Dialog dialog = alert.Create();
            dialog.Show();

        }
        //protected override void OnResume()
        //{
        //    myPagerAdapter.NotifyDataSetChanged();
        //}
    }
}