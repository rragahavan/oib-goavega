﻿using Android.App;
using Android.Widget;
using Android.OS;
using OfficeTools.Employees.Droid.Views;
using V7Toolbar = Android.Support.V7.Widget.Toolbar;

namespace OfficeTools.Employees.Droid
{
    [Activity(Theme = "@style/Theme.DesignDemo", Label = "OfficeTools.Employees.Droid")]
    public class HomePageActivity : NavigationDrawerActivity
    {
        
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.Main);
            Set(Resources.GetStringArray(Resource.Array.nav_drawer_items), Resources.ObtainTypedArray(Resource.Array.nav_drawer_icons));
            var toolbar = FindViewById<V7Toolbar>(Resource.Id.toolbar);
            SetSupportActionBar(toolbar);
            TextView toolbarTitle = FindViewById<TextView>(Resource.Id.toolbarTitle);
            toolbarTitle.Text = Resources.GetString(Resource.String.Home);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);
            SupportActionBar.SetDisplayShowTitleEnabled(false);
            SupportActionBar.SetHomeButtonEnabled(true);
            SupportActionBar.SetHomeAsUpIndicator(Resource.Drawable.ic_hamburger);
        }

        public override void OnBackPressed()
        {
            
            Finish();
        }
    }
}

