using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace OfficeTools.Employees.Droid.Classes
{
    public static class AppUtil
    {
        public static bool IsNetworAvailable(Context context)
        {
            Android.Net.ConnectivityManager connectivityManager =(Android.Net.ConnectivityManager)context.GetSystemService(Context.ConnectivityService);
            Android.Net.NetworkInfo activeConnection = connectivityManager.ActiveNetworkInfo;
            return (activeConnection != null) && activeConnection.IsConnected;
        }

    }
}