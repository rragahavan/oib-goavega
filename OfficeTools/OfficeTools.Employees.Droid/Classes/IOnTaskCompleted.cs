namespace OfficeTools.Employees.Droid.Classes
{
    public interface IOnTaskCompleted
    {
        void OnTaskCompleted(Java.Lang.Object result);
    }
}