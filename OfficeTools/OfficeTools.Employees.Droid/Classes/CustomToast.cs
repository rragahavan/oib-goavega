using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace OfficeTools.Employees.Droid.Classes
{
    public class CustomToast : Toast
    {
        public CustomToast(Context context)
            : base(context)
        {

        }

        public CustomToast(Context context, Activity activity, string message, bool flag)
            : base(context)
        {
            LayoutInflater inflater = activity.LayoutInflater;
            View view = inflater.Inflate(Resource.Layout.CustomToastLayout, null);
            TextView txt = view.FindViewById<TextView>(Resource.Id.customToastText);
            txt.Text = message;
            SetGravity(GravityFlags.Top, 0, 0);
            Duration = ToastLength.Long;
            View = view;
        }
    }
}
