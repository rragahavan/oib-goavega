using Android.Content;
using Android.Runtime;
using Android.Support.V4.App;
using Android.Views;
using Android.Widget;
using Java.Lang;
using OfficeTools.Employees.Droid.Fragments;

namespace OfficeTools.Employees.Droid.Adapters
{
    public class LeavePagerAdapter : Android.Support.V4.App.FragmentPagerAdapter
    {
        private static int NUM_ITEMS = 4;
        private readonly string[] tabTitles;// = { "My Leave", "Apply Leave","My Summary","Leave Request" };
        private readonly string employeeId;
        private Context context;

        public LeavePagerAdapter(FragmentManager fm, string[] tabTitles,Context context) : base(fm)
        {
            this.tabTitles = tabTitles;
            //this.employeeId = employeeId;
            this.context = context;
        }

        public override int Count
        {
            get
            {
                
                return tabTitles.Length; 
            }
        }

        public override ICharSequence GetPageTitleFormatted(int position)
        {

            return CharSequence.ArrayFromStringArray(tabTitles)[position];

        }

        public View getTabView(int position)
        {
            View tab = LayoutInflater.From(context).Inflate(Resource.Layout.CustomTabTitle, null);
            TextView tv = (TextView)tab.FindViewById(Resource.Id.custom_text);
            
            tv.Text=tabTitles[position];
            return tab;
        }




        public override Fragment GetItem(int position)
        {
            switch (position)
            {
                case 0:
                    //        return MyLeaveFragment.NewInstance(employeeId);
                    return new MyLeaveFragment();
                case 1:
                    // return ApplyLeaveFragment.NewInstance(employeeId);
                    return new ApplyLeaveFragment();
                case 2:
                    return new LeaveSummaryFragment();
                case 3:
                    return new LeaveRequestFragment();
                default:
                    return null;
            }
        }
    }
}