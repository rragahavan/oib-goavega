using Android.OS;
using Android.Views;
using Android.Widget;
using Android.Support.V4.App;
using OfficeTools.Employees.Droid.Classes;

namespace OfficeTools.Employees.Droid.Fragments
{
    public class LeaveSummaryFragment : Fragment
    {
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            View view = inflater.Inflate(Resource.Layout.LeaveSummaryLayout, container, false);
            TextView leaveTypetText = (view).FindViewById<TextView>(Resource.Id.leaveTypeText);
            TextView totalText = (view).FindViewById<TextView>(Resource.Id.totalText);
            TextView appliedText = (view).FindViewById<TextView>(Resource.Id.appliedText);
            TextView balancetext = (view).FindViewById<TextView>(Resource.Id.balanceText);

            if (AppUtil.IsNetworAvailable(this.Activity))
            {
                var response = new Repositories.EmployeeRepository(this.Activity).GetLeaveDetails();
                if (response.Success)
                {
                    leaveTypetText.Text = response.Data.leaveSummary.leavesummary[0].leaveType.Description;
                    totalText.Text = "Total: " + response.Data.leaveSummary.leavesummary[0].total.ToString();
                    appliedText.Text = "Applied Leave: " + (response.Data.leaveSummary.leavesummary[0].total - response.Data.leaveSummary.leavesummary[0].balance).ToString();
                    balancetext.Text = "Balance Leave: " + response.Data.leaveSummary.leavesummary[0].balance.ToString();

                }
            }
            else
                display(GetString(Resource.String.NoInternetConnection));
            return view;
            
        }
        public void display(string toastMeassage)
        {
            CustomToast customMessage = new CustomToast(this.Activity, this.Activity, toastMeassage, true);
            customMessage.SetGravity(GravityFlags.Bottom, 0, 0);
            customMessage.Show();

        }
    }
}