using System;
using System.Collections.Generic;
using Android.Content;
using Android.OS;
using Android.Views;
using Android.Widget;
using Android.Support.V4.App;
using OfficeTools.Shared.Models;
using Android.Support.V7.Widget;
using OfficeTools.Employees.Droid.Classes;
using System.Linq;

namespace OfficeTools.Employees.Droid.Fragments
{
    public class LeaveRequestFragment : Fragment
    {
        private List<ManagerleaveRequest> leaveDetails = new List<ManagerleaveRequest>();
        private RecyclerView mRecyclerView;
        RecyclerView.LayoutManager mLayoutManager;
        LeavesRequestAdapter mAdapter;
        CheckBox selectCheckbox2;
        String leaveActionType;
        private Context context;

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

        }

        public override void OnResume()
        {
            base.OnResume();
            if (AppUtil.IsNetworAvailable(this.Activity))
            {
                leaveDetails = new Repositories.EmployeeRepository(this.Activity).GetLeaveDetails().Data.ManagerleaveRequests;
                mAdapter.NotifyDataSetChanged();
            }
            else
                Display(GetString(Resource.String.NoInternetConnection));
        }


        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            View view = inflater.Inflate(Resource.Layout.MyLeaveRequest, container, false);
            context = this.Activity;
            if (AppUtil.IsNetworAvailable(this.Activity))
            {
                leaveDetails = new Repositories.EmployeeRepository(this.Activity).GetLeaveDetails().Data.ManagerleaveRequests;
                Spinner leaveStatusSpinner = view.FindViewById<Spinner>(Resource.Id.statusOfLeave);
                leaveStatusSpinner.ItemSelected += LeaveTypeSpinner_ItemSelected;
                var adapter = ArrayAdapter.CreateFromResource(
                    this.Activity, Resource.Array.leave_status, Resource.Layout.Spinner_item);
                adapter.SetDropDownViewResource(Android.Resource.Layout.SimpleSpinnerDropDownItem);
                leaveStatusSpinner.Adapter = adapter;

                Spinner leaveActionSpinner = view.FindViewById<Spinner>(Resource.Id.leaveType);
                leaveActionSpinner.ItemSelected += LeaveActionSpinner_ItemSelected;
                var leaveActionAdapter = ArrayAdapter.CreateFromResource(
                    this.Activity, Resource.Array.action_type, Resource.Layout.Spinner_item);
                adapter.SetDropDownViewResource(Android.Resource.Layout.SimpleSpinnerDropDownItem);
                leaveActionSpinner.Adapter = leaveActionAdapter;
                selectCheckbox2 = view.FindViewById<CheckBox>(Resource.Id.selectCheckbox);
                mRecyclerView = view.FindViewById<RecyclerView>(Resource.Id.recyclerView);
                mLayoutManager = new LinearLayoutManager(this.Activity);
                mRecyclerView.SetLayoutManager(mLayoutManager);
                mAdapter = new LeavesRequestAdapter(this.Activity, leaveDetails, context, "");
                mRecyclerView.SetAdapter(mAdapter);
                mAdapter.NotifyDataSetChanged();
            }
            else
                Display(GetString(Resource.String.NoInternetConnection));
            return view;
        }

        private void LeaveActionSpinner_ItemSelected(object sender, AdapterView.ItemSelectedEventArgs e)
        {
            Spinner spinner = (Spinner)sender;
            leaveActionType = spinner.GetItemAtPosition(e.Position).ToString();
            if (!leaveActionType.Equals("Action") && selectCheckbox2.Checked.Equals(true))
            {
                var leaveDetailStatus = leaveDetails;
                leaveDetailStatus = leaveDetailStatus.Where(i => i?.status == leaveActionType).ToList();

                mAdapter = new LeavesRequestAdapter(this.Activity, leaveDetailStatus, context, leaveActionType);
                mRecyclerView.SetAdapter(mAdapter);
                mAdapter.NotifyDataSetChanged();
                var idList = leaveDetailStatus.Select(i => i._id).ToList();

                var builder = new Android.Support.V7.App.AlertDialog.Builder(context);
                var alert = builder.Create();
                View itemView1 = LayoutInflater.From(context).
                        Inflate(Resource.Layout.custom_dialog, null, false);
                alert.SetView(itemView1);
                alert.Show();
                EditText commentText = (itemView1).FindViewById<EditText>(Resource.Id.commentText);
                Button acceptButton = (itemView1).FindViewById<Button>(Resource.Id.acceptButton);
                TextView closeButton = (itemView1).FindViewById<TextView>(Resource.Id.closeButton);
                closeButton.Click += delegate { alert.Cancel(); };
                acceptButton.Click += delegate
                {
                    alert.Cancel();
                    var leaveData = new LeaveRequest { id = idList[0], managerComments = commentText.Text.ToString(), status = "approved" };
                    //if (AppUtil.IsNetworAvailable(context))
                    //{
                    //    var response = new Repositories.EmployeeRepository(context).UpadteAllLeavestatus(leaveData);
                    //    if (response.Success)
                    //    {

                    //    }
                    //}
                };
            }
        }

        private void LeaveTypeSpinner_ItemSelected(object sender, AdapterView.ItemSelectedEventArgs e)
        {
            Spinner spinner = (Spinner)sender;
            var leaveTypeValue = spinner.GetItemAtPosition(e.Position).ToString();
            if (!leaveTypeValue.Equals("All"))
            {
                var leaveDetailStatus = leaveDetails;
                leaveDetailStatus = leaveDetailStatus.Where(i => i?.status == leaveTypeValue).ToList();
                mAdapter = new LeavesRequestAdapter(this.Activity, leaveDetailStatus, context, leaveTypeValue);
                mRecyclerView.SetAdapter(mAdapter);
                mAdapter.NotifyDataSetChanged();
                
            }
            else
            {
                mAdapter = new LeavesRequestAdapter(this.Activity, leaveDetails, context, "");
                mRecyclerView.SetAdapter(mAdapter);
                mAdapter.NotifyDataSetChanged();

            }
        }

        public class LeaveViewHolder : RecyclerView.ViewHolder
        {

            public TextView StartDate { get; private set; }
            public TextView EndDate { get; private set; }
            public TextView LeaveType { get; private set; }
            public TextView Days { get; private set; }
            public TextView Status { get; private set; }
            public LinearLayout statusLeaveLayout { get; private set; }
            public LinearLayout linearOneLayout { get; private set; }
            public LinearLayout linearTwoLayout { get; private set; }
            public LinearLayout linearThreeLayout { get; private set; }
            public LinearLayout LeaveStatusLayout { get; private set; }
            public TextView Days1 { get; private set; }
            public TextView Status1 { get; private set; }
            public TextView ApproveText { get; private set; }
            public TextView CancelText { get; private set; }
            public CardView parentLayout { get; private set; }
            public TextView EmployeeName { get; private set; }


            public LeaveViewHolder(View itemView, Action<int> listener)
                : base(itemView)
            {
                StartDate = itemView.FindViewById<TextView>(Resource.Id.startDateText);
                EndDate = itemView.FindViewById<TextView>(Resource.Id.endDateText);
                LeaveType = itemView.FindViewById<TextView>(Resource.Id.leaveTypeText);
                Days = itemView.FindViewById<TextView>(Resource.Id.daysText);
                Status = itemView.FindViewById<TextView>(Resource.Id.statusText);
                Days1 = itemView.FindViewById<TextView>(Resource.Id.daysText1);
                Status1 = itemView.FindViewById<TextView>(Resource.Id.statusText1);
                ApproveText = (itemView).FindViewById<TextView>(Resource.Id.approveText);
                CancelText = (itemView).FindViewById<TextView>(Resource.Id.cancelText);
                linearOneLayout = itemView.FindViewById<LinearLayout>(Resource.Id.linearOne);
                linearTwoLayout = itemView.FindViewById<LinearLayout>(Resource.Id.linearFour);
                LeaveStatusLayout = itemView.FindViewById<LinearLayout>(Resource.Id.statusLeaveLayout);
                parentLayout = itemView.FindViewById<CardView>(Resource.Id.parentCardView);
                EmployeeName = itemView.FindViewById<TextView>(Resource.Id.employeeNameText);
            }
        }

        public class LeavesRequestAdapter : RecyclerView.Adapter
        {
            public event EventHandler<int> ItemClick;
            public List<ManagerleaveRequest> mLeave;
            public Context context;
            public object filterOption;
            public FragmentActivity activity;

            public LeavesRequestAdapter(FragmentActivity activity, List<ManagerleaveRequest> leave, Context context, object filterOption = null)
            {
                mLeave = leave;
                this.context = context;
                this.filterOption = filterOption;
                this.activity = activity;
            }

            public override RecyclerView.ViewHolder
                OnCreateViewHolder(ViewGroup parent, int viewType)
            {
                View itemView = LayoutInflater.From(parent.Context).
                            Inflate(Resource.Layout.MyLeaveIRequestItemsRecycler, parent, false);
                LeaveViewHolder vh = new LeaveViewHolder(itemView, OnClick);
                return vh;
            }
            public override int GetItemViewType(int position)
            {
                return position;
            }

            public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
            {
                LeaveViewHolder vh = holder as LeaveViewHolder;
                if (position % 2 != 0)
                {
                    vh.linearOneLayout.Visibility = ViewStates.Gone;
                    vh.linearTwoLayout.Visibility = ViewStates.Visible;
                }
                var leave = mLeave[position];
                if (!filterOption.Equals(""))
                {
                    if (!filterOption.Equals(leave?.status))
                    {
                        vh.parentLayout.Visibility = ViewStates.Gone;
                    }

                }
                vh.EndDate.Text = Convert.ToDateTime(leave.toDate).ToString("MMM dd, yyyy");
                vh.StartDate.Text = Convert.ToDateTime(leave.fromDate).ToString("MMM dd, yyyy");
                vh.Days.Text = leave?.numDays.ToString();
                vh.Days1.Text = leave?.numDays.ToString();
                vh.LeaveType.Text = "Casual Leave";
                vh.EmployeeName.Text = leave.User?.FirstName;
                //vh.LeaveType.Text = leave?.leaveType;
                vh.ApproveText.Click += delegate
                {
                    var builder = new Android.Support.V7.App.AlertDialog.Builder(context);
                    var alert = builder.Create();
                    View itemView1 = LayoutInflater.From(context).
                            Inflate(Resource.Layout.custom_dialog, null, false);
                    alert.SetView(itemView1);
                    alert.Show();
                    EditText commentText = (itemView1).FindViewById<EditText>(Resource.Id.commentText);
                    Button acceptButton = (itemView1).FindViewById<Button>(Resource.Id.acceptButton);
                    TextView closeButton = (itemView1).FindViewById<TextView>(Resource.Id.closeButton);

                    closeButton.Click += delegate { alert.Cancel(); };

                    acceptButton.Click += delegate
                    {
                        alert.Cancel();
                        var leaveData = new LeaveRequest { id = mLeave[position]._id, managerComments = commentText.Text.ToString(), status = "approved" };
                        if (AppUtil.IsNetworAvailable(context))
                        {
                            var response = new Repositories.EmployeeRepository(context).UpadteLeavestatus(leaveData);
                            if (response.Success)
                                DisplayToastMeassage(context.GetString(Resource.String.LeaveStatusUpdated));
                        }
                        else
                           DisplayToastMeassage(context.GetString(Resource.String.NoInternetConnection));
                    };
                };

                vh.CancelText.Click += delegate
                {
                    var builder = new Android.Support.V7.App.AlertDialog.Builder(context);
                    var alert = builder.Create();
                    View itemView1 = LayoutInflater.From(context).
                            Inflate(Resource.Layout.custom_dialog, null, false);
                    alert.SetView(itemView1);
                    alert.Show();
                    EditText commentText = (itemView1).FindViewById<EditText>(Resource.Id.commentText);
                    Button acceptButton = (itemView1).FindViewById<Button>(Resource.Id.acceptButton);
                    TextView closeButton = (itemView1).FindViewById<TextView>(Resource.Id.closeButton);
                    closeButton.Click += delegate
                    {
                        alert.Cancel();
                    };
                    acceptButton.Click += delegate
                    {
                        alert.Cancel();
                        if (AppUtil.IsNetworAvailable(context))
                        {
                            var leaveData = new LeaveRequest { id = mLeave[position]._id, managerComments = commentText.Text.ToString(), status = "rejected" };
                            var response = new Repositories.EmployeeRepository(context).UpadteLeavestatus(leaveData);
                            if (response.Success)
                                DisplayToastMeassage(context.GetString(Resource.String.LeaveStatusUpdated));
                        }
                        else
                            DisplayToastMeassage(context.GetString(Resource.String.NoInternetConnection));
                    };
                };

                var s = leave?.status;
                if (s.Equals("approved"))
                {
                    vh.Status.SetCompoundDrawablesWithIntrinsicBounds(Resource.Drawable.ic_action_check_mark_white, 0, 0, 0);
                    vh.Status1.SetCompoundDrawablesWithIntrinsicBounds(Resource.Drawable.ic_action_check_mark_white, 0, 0, 0);
                }
                else if (s.Equals("open"))
                {
                    vh.Status.SetCompoundDrawablesWithIntrinsicBounds(Resource.Drawable.ic_action_open, 0, 0, 0);
                    vh.Status1.SetCompoundDrawablesWithIntrinsicBounds(Resource.Drawable.ic_action_open, 0, 0, 0);
                    vh.LeaveStatusLayout.Visibility = ViewStates.Visible;
                    vh.linearOneLayout.SetBackgroundColor(context.Resources.GetColor(Resource.Color.LightOrange));
                    vh.linearTwoLayout.SetBackgroundColor(context.Resources.GetColor(Resource.Color.LightOrange));

                }
                else
                {
                    vh.Status.SetCompoundDrawablesWithIntrinsicBounds(Resource.Drawable.ic_action_rejected, 0, 0, 0);
                    vh.Status1.SetCompoundDrawablesWithIntrinsicBounds(Resource.Drawable.ic_action_rejected, 0, 0, 0);
                    vh.linearOneLayout.SetBackgroundColor(context.Resources.GetColor(Resource.Color.DarkRed));
                    vh.linearTwoLayout.SetBackgroundColor(context.Resources.GetColor(Resource.Color.DarkRed));

                }
            }

            public override int ItemCount
            {
                get { return mLeave.Count; }
            }

            void OnClick(int position)
            {
                ItemClick?.Invoke(this, position);

            }

            public void DisplayToastMeassage(string toastMeassage)
            {
                CustomToast customMessage = new CustomToast(activity, activity, toastMeassage, true);
                customMessage.SetGravity(GravityFlags.Bottom, 0, 0);
                customMessage.Show();
            }

        }
        public void Display(string toastMeassage)
        {
            CustomToast customMessage = new CustomToast(this.Activity, this.Activity, toastMeassage, true);
            customMessage.SetGravity(GravityFlags.Bottom, 0, 0);
            customMessage.Show();
        }
    }
}