using Android.Content;
using Android.OS;
using Android.Support.V4.App;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using OfficeTools.Employees.Droid.Classes;
using OfficeTools.Shared.Models;
using System;
using System.Collections.Generic;

namespace OfficeTools.Employees.Droid.Fragments
{
    public class MyLeaveFragment : Fragment
    {

        private List<LeaveRequest> leaveDetails = new List<LeaveRequest>();
        private RecyclerView mRecyclerView;
        RecyclerView.LayoutManager mLayoutManager;
        LeaveAdapter mAdapter;
        private Context context;


        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
        }

        public override void OnResume()
        {
            base.OnResume();
            if (AppUtil.IsNetworAvailable(this.Activity))
            {
                leaveDetails = new Repositories.EmployeeRepository(this.Activity).GetLeaveDetails().Data.LeaveRequests;
                mAdapter.NotifyDataSetChanged();
            }
            else
                Display(GetString(Resource.String.NoInternetConnection));
        }


        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            View view = inflater.Inflate(Resource.Layout.MyLeaveLayout, container, false);
            context = this.Activity;
            if (AppUtil.IsNetworAvailable(this.Activity))
            {
                leaveDetails = new Repositories.EmployeeRepository(this.Activity).GetLeaveDetails().Data.LeaveRequests;
                mRecyclerView = view.FindViewById<RecyclerView>(Resource.Id.recyclerView);
                mLayoutManager = new LinearLayoutManager(this.Activity);
                mRecyclerView.SetLayoutManager(mLayoutManager);
                mAdapter = new LeaveAdapter(leaveDetails, context);
                mRecyclerView.SetAdapter(mAdapter);
                
                mAdapter.NotifyDataSetChanged();
            }
            else
                Display(GetString(Resource.String.NoInternetConnection));
            return view;
        }


        public class LeaveViewHolder : RecyclerView.ViewHolder
        {

            public TextView StartDate { get; private set; }
            public TextView EndDate { get; private set; }
            public TextView LeaveType { get; private set; }
            public TextView Days { get; private set; }
            public TextView Status { get; private set; }
            public LinearLayout statusLeaveLayout { get; private set; }
            public LinearLayout linearOneLayout { get; private set; }
            public LinearLayout linearTwoLayout { get; private set; }
            public LinearLayout linearThreeLayout { get; private set; }
            public TextView Days1 { get; private set; }
            public TextView Status1 { get; private set; }

            public LeaveViewHolder(View itemView, Action<int> listener)
                : base(itemView)
            {

                StartDate = itemView.FindViewById<TextView>(Resource.Id.startDateText);
                EndDate = itemView.FindViewById<TextView>(Resource.Id.endDateText);
                LeaveType = itemView.FindViewById<TextView>(Resource.Id.leaveTypeText);
                Days = itemView.FindViewById<TextView>(Resource.Id.daysText);
                Status = itemView.FindViewById<TextView>(Resource.Id.statusText);
                Days1 = itemView.FindViewById<TextView>(Resource.Id.daysText1);
                Status1 = itemView.FindViewById<TextView>(Resource.Id.statusText1);
                linearOneLayout = itemView.FindViewById<LinearLayout>(Resource.Id.linearOne);
                linearTwoLayout = itemView.FindViewById<LinearLayout>(Resource.Id.linearFour);
            }
        }

        public class LeaveAdapter : RecyclerView.Adapter
        {
            public event EventHandler<int> ItemClick;
            public List<LeaveRequest> mLeave;
            public Context context;

            public LeaveAdapter(List<LeaveRequest> leave, Context context)
            {
                mLeave = leave;
                this.context = context;
            }

            public override RecyclerView.ViewHolder
                OnCreateViewHolder(ViewGroup parent, int viewType)
            {
                View itemView = LayoutInflater.From(parent.Context).
                            Inflate(Resource.Layout.MyLeaveItemsRecycler, parent, false);
                LeaveViewHolder vh = new LeaveViewHolder(itemView, OnClick);
                return vh;
            }
            public override int GetItemViewType(int position)
            {
                return position;
            }

            public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
            {
                LeaveViewHolder vh = holder as LeaveViewHolder;
                if (position % 2 != 0)
                {
                    vh.linearOneLayout.Visibility = ViewStates.Gone;
                    vh.linearTwoLayout.Visibility= ViewStates.Visible; 
                }
                var leave = mLeave[position];
                if (leave.status.Equals("open"))
                {

                }
                vh.EndDate.Text = Convert.ToDateTime(leave.toDate).ToString("MMM dd, yyyy");
                vh.StartDate.Text = Convert.ToDateTime(leave.fromDate).ToString("MMM dd, yyyy");
                vh.Days.Text = leave?.numDays.ToString();
                vh.Days1.Text = leave?.numDays.ToString();
                vh.LeaveType.Text = "Casual Leave";
                var s = leave?.status;
                if (s.Equals("approved"))
                {
                    vh.Status.SetCompoundDrawablesWithIntrinsicBounds(Resource.Drawable.ic_action_check_mark_white, 0, 0, 0);
                    vh.Status1.SetCompoundDrawablesWithIntrinsicBounds(Resource.Drawable.ic_action_check_mark_white, 0, 0, 0);
                }
                else if (s.Equals("open"))
                {
                    vh.Status.SetCompoundDrawablesWithIntrinsicBounds(Resource.Drawable.ic_action_open, 0, 0, 0);
                    vh.Status1.SetCompoundDrawablesWithIntrinsicBounds(Resource.Drawable.ic_action_open, 0, 0, 0);
                    vh.linearOneLayout.SetBackgroundColor(context.Resources.GetColor(Resource.Color.LightOrange));
                    vh.linearTwoLayout.SetBackgroundColor(context.Resources.GetColor(Resource.Color.LightOrange));
                }
                else
                {
                    vh.Status.SetCompoundDrawablesWithIntrinsicBounds(Resource.Drawable.ic_action_rejected, 0, 0, 0);
                    vh.Status1.SetCompoundDrawablesWithIntrinsicBounds(Resource.Drawable.ic_action_rejected, 0, 0, 0);
                    vh.linearOneLayout.SetBackgroundColor(context.Resources.GetColor(Resource.Color.DarkRed));
                    vh.linearTwoLayout.SetBackgroundColor(context.Resources.GetColor(Resource.Color.DarkRed));
                }
            }


            public override int ItemCount
            {
                get { return mLeave.Count; }
            }


            void OnClick(int position)
            {
                ItemClick?.Invoke(this, position);
            }
        }

        public void Display(string toastMeassage)
        {
            CustomToast customMessage = new CustomToast(this.Activity, this.Activity, toastMeassage, true);
            customMessage.SetGravity(GravityFlags.Bottom, 0, 0);
            customMessage.Show();
        }
    }
}
