using Android.Content;
using Newtonsoft.Json;
using OfficeTools.Shared;
using OfficeTools.Shared.Common;
using OfficeTools.Shared.Models;
using OfficeTools.Shared.Response;
using RestSharp;
using System;

namespace OfficeTools.Employees.Droid.Repositories
{
    public class EmployeeRepository : BaseRepository
    {
        private readonly Context context;

        public EmployeeRepository(Context context)
        {
            this.context = context;
        }

        public EmployeeListResponse GetEmployeeList()
        {
            EmployeeListResponse empList;
            var response = InvokeApi("hrms", "employeelist");
            empList = JsonConvert.DeserializeObject<EmployeeListResponse>(response.Content);
            return empList;
        }

        public DashboardResponse GetDashboardDetails()
        {
            DashboardResponse dashboradList;
            var response= InvokeApi("hrms", "getDashboardData");
            dashboradList = JsonConvert.DeserializeObject<DashboardResponse>(response.Content);
            return dashboradList;
        }
        public EmployeeResponse GetEmployeeDetails(string empCode)
        {
            EmployeeResponse empList;
            var parameter = new { empcode = empCode };
            var response = InvokeApi("hrms", "getemployee", parameter);
            empList = JsonConvert.DeserializeObject<EmployeeResponse>(response.Content);
            return empList;
        }

        public LoginResponse GetAccessToken(string accessToken)
        {
            LoginResponse loginResponse;
            //var client = new RestClient(new Uri(ApiService.BASE_URL));
            var client = new RestClient(new Uri(ApiService.LOCAL_BASE_URL));
            var parameter = new {token= "\"" + accessToken + "\"" };
            var request = new RestRequest("api/login", Method.POST);
            request.AddJsonBody(parameter);
            var respone = client.Execute(request);
            loginResponse = JsonConvert.DeserializeObject<LoginResponse>(respone.Content);
            return loginResponse; 
        }

        public LeaveResponse GetLeaveDetails()
        {
            LeaveResponse leaveList;            
            var response = InvokeApi("lms", "getLeaves");
            leaveList = JsonConvert.DeserializeObject<LeaveResponse>(response.Content);
            return leaveList;
        }

        public LeaveResponse AddLeaveDetails(LeaveRequest leave)
        {
            LeaveResponse leaveList;            
            var leaveObject = new { leave };
            var parameter = leaveObject;
            var response = InvokeApi("lms","applyLeave",parameter);
            leaveList = JsonConvert.DeserializeObject<LeaveResponse>(response.Content);
            return leaveList;
        }

        public LeaveResponse UpadteLeavestatus(LeaveRequest leaveDetail)
        {
            LeaveResponse leaveList;
            var leaveObject = new { leaveDetail };
            var parameter = leaveObject;
            var response = InvokeApi("lms", "changeStatus", parameter);
            leaveList = JsonConvert.DeserializeObject<LeaveResponse>(response.Content);
            return leaveList;
        }

        public LeaveResponse UpadteAllLeavestatus(LeaveRequest leaveDetail)
        {
            LeaveResponse leaveList;
            var leaveObject = new { leaveDetail };
            var parameter = leaveObject;
            var response = InvokeApi("lms", "changeStatus", parameter);
            leaveList = JsonConvert.DeserializeObject<LeaveResponse>(response.Content);
            return leaveList;
        }



    }
}