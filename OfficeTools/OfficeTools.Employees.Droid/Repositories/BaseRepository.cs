using System;
using OfficeTools.Shared.Common;
using RestSharp;
using OfficeTools.Shared;
 
using OfficeTools.Employees.Droid.Classes;

namespace OfficeTools.Employees.Droid.Repositories
{
    public class BaseRepository
    {
        public IRestResponse InvokeApi(string service, string method, object parameter=null)
        {
            //var client = new RestClient(new Uri(ApiService.BASE_URL));
            var client = new RestClient(new Uri(ApiService.LOCAL_BASE_URL));
            ServiceRequest serviceRequest = new ServiceRequest(service, method,parameter);
            var request = new RestRequest("api/invoke", Method.POST);
            string Authorization = "\"" + AppPreferences.GetAccessToken() + "\"";
            request.AddHeader("Authorization", Authorization);
            request.AddJsonBody(serviceRequest);
            return client.Execute(request);
        }
    }
}