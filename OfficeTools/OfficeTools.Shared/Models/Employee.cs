﻿
using Newtonsoft.Json;
using System.Collections.Generic;

namespace OfficeTools.Shared
{
    public class PresentAddress
    {
        [JsonProperty("address")]
        public string Address { get; set; }
        [JsonProperty("city")]
        public string City { get; set; }
        [JsonProperty("state")]
        public string State { get; set; }
        [JsonProperty("country")]
        public string Country { get; set; }
        [JsonProperty("zip")]
        public string Zip { get; set; }
    }

    public class PermanentAddress
    {
        [JsonProperty("address")]
        public string Address { get; set; }
        [JsonProperty("city")]
        public string City { get; set; }
        [JsonProperty("state")]
        public string State { get; set; }
        [JsonProperty("zip")]
        public string Zip { get; set; }
        [JsonProperty("country")]
        public string Country { get; set; }
        
    }

    public class Employee
    {
        [JsonProperty("_id")]
        public string Id { get; set; }
        [JsonProperty("skills")]
        public List<object> Skills { get; set; }
        [JsonProperty("qualifications")]
        public List<object> Qualifications { get; set; }
        [JsonProperty("experiences")]
        public List<object> Experiences { get; set; }
        [JsonProperty("presentAddress")]
        public PresentAddress PresentAddress { get; set; }
        [JsonProperty("permanentAddress")]
        public PermanentAddress PermanentAddress { get; set; }
        [JsonProperty("jobTitle")]
        public string JobTitle { get; set; }
        [JsonProperty("empCode")]
        public string EmpCode { get; set; }
        [JsonProperty("email")]
        public string Email { get; set; }
        [JsonProperty("firstName")]
        public string FirstName { get; set; }
        [JsonProperty("lastName")]
        public string LastName { get; set; }
        [JsonProperty("roles")]
        public List<string> Roles { get; set; }
        [JsonProperty("managerId")]
        public string ManagerId { get; set; }
        [JsonProperty("joiningDate")]
        public string JoiningDate { get; set; }
        [JsonProperty("empType")]
        public string EmpType { get; set; }
        [JsonProperty("shiftType")]
        public string ShiftType { get; set; }
        [JsonProperty("grade")]
        public string Grade { get; set; }
        [JsonProperty("notes")]
        public string Notes { get; set; }
        [JsonProperty("profileImage")]
        public string ProfileImage { get; set; }
        [JsonProperty("mobile")]
        public string Mobile { get; set; }
        [JsonProperty("releaveDate")]
        public string ReleaveDate { get; set; }
    }
    
}
