﻿using Newtonsoft.Json;
using OfficeTools.Shared.Models;

namespace OfficeTools.Shared.Response
{
    public class LeaveResponse

    {
        [JsonProperty("success")]
        public bool Success { get; set; }
        [JsonProperty("data")]
        public Leave Data { get; set; }
        [JsonProperty("ok")]
        public int Ok { get; set; }
        [JsonProperty("n")]
        public int N { get; set; }
    }
}
