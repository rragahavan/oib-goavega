﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace OfficeTools.Shared
{
    public class EmployeeListResponse
    {
        [JsonProperty("success")]
        public bool Success { get; set; }
        [JsonProperty("data")]
        public List<Employee> Data { get; set; }
    }
}
