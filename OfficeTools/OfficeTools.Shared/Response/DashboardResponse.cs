﻿using OfficeTools.Shared.Models;

namespace OfficeTools.Shared.Response
{
    public class DashboardResponse
    {
        public bool success { get; set; }
        public Dashboard data { get; set; }
    }
}
