﻿using Newtonsoft.Json;

namespace OfficeTools.Shared
{
    public class ServiceRequest
    {
        public string service { get; set; }
        public string action { get; set; }
        public dynamic parameter { get; set; }

        public ServiceRequest(string service, string action, dynamic parameter = null)
        {
            this.service = service;
            this.action = action;
            this.parameter = parameter;
        }

    }
}
