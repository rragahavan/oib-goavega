﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OfficeTools.Shared
{
    public class ServiceRequest
    {
        public ServiceRequest(string service, string action, dynamic parameter=null)
        {
            this.service = service;
            this.action = action;
            this.parameter = parameter;
        }

        public string service { get; set; }
        public string action { get; set; }
        public dynamic parameter { get; set; }
    }
}
